<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Songtian.com</title>
<link href="${pageContext.request.contextPath}/css/main.css" rel="stylesheet" type="text/css" />
</head>
<body>
<%@include file="header.jsp" %>
<div class="bo">
	<div class="word">
		<h2>缤纷四季优惠搜不停 </h2>
		<h4>从舒适的乡村民宿到时髦的都市公寓 </h4>
	</div>
	<form action="${pageContext.request.contextPath}/Main_HotelLike">
		<input type="text"  class="text" placeholder="请输入酒店名称" autocomplete="off"data-component="search/destination/input-placeholder" name="hotelname" value="<c:if test="${!empty sessionScope.hotelname }">${sessionScope.hotelname }</c:if>" ><input type="submit" value="搜索" class="rag">
    </form>
    <table>
    	<tr>
    		<c:forEach items="${requestScope.userAll}" var="t" end="2"  >
    			<td><div><a href="${pageContext.request.contextPath}/Room_HotelAll?hotelName=${t.HB_id}"><img class="images" src="${pageContext.request.contextPath}/displayImg?qqq=hotel_base&type=HB_id&text=${t.HB_id}&ss=HB_photo"><p>${t.HB_id}</p><p>${t.HB_url}</p></a></div></td>
    		</c:forEach>
    	</tr>
    	<tr>
    		<c:forEach items="${requestScope.userAll}" var="t" begin="3"   end="5" >
    			<td><div><a href="${pageContext.request.contextPath}/Room_HotelAll?hotelName=${t.HB_id}"><img class="images" src="${pageContext.request.contextPath}/displayImg?qqq=hotel_base&type=HB_id&text=${t.HB_id}&ss=HB_photo"><p>${t.HB_id}</p><p>${t.HB_url}</p></a></div></td>
    		</c:forEach>
    	</tr>
    	<tr>
    		<c:forEach items="${requestScope.userAll}" var="t" begin="6"   end="8" >
    			<td><div><a href="${pageContext.request.contextPath}/Room_HotelAll?hotelName=${t.HB_id}"><img class="images" src="${pageContext.request.contextPath}/displayImg?qqq=hotel_base&type=HB_id&text=${t.HB_id}&ss=HB_photo"><p>${t.HB_id}</p><p>${t.HB_url}</p></a></div></td>
    		</c:forEach>
    	</tr>
    </table>
</div>
<%@include file="footer.jsp" %>
</body>
</html>