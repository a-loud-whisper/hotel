<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<link href="${pageContext.request.contextPath}/css/header.css" rel="stylesheet" type="text/css" />
<div class="header1">
	<div class="cent">
		<div class="left3">
   			<h1>
   			<font color="white">Songtian</font><font color="#44a9df">.com</font> 
   			</h1>
 		</div>
 		<div class="right1">
 			
 			<ul>
  			 <c:choose>
  			 	<c:when test="${empty sessionScope.guest }">
  					 <li> <a href="${pageContext.request.contextPath}/jsp/register.jsp"><input type="submit" class="radio1" value="注冊"></a></li>
  					 <li><a href="${pageContext.request.contextPath}/jsp/login.jsp"><input type="submit" class="radio1" value="登录"></a></li>
  			 	</c:when>
  			 	<c:otherwise>
  			 		<li><a href="${pageContext.request.contextPath}/Guest_base?phone=${sessionScope.guest.HG_phone}"><p1>尊敬的${sessionScope.guest.HG_name}，您好</p1></a></li>
  			 		 <li><a href="${pageContext.request.contextPath}/GuestLoginOut"><input type="submit" class="radio1" value="注销"></a></li>
  			 	</c:otherwise>
  			 </c:choose>
    	</ul></div>
 	<div class="hotel">
   		 <ul>
  			 <li><a href="#">住宿</a></li>
    		 <li><a href="#">机票</a></li>
    		 <li><a href="#">租车</a></li>
    		 <li><a href="#">观光和活动</a></li>
    		 <li><a href="#">机场出租车</a></li>
    	</ul>
</div></div>
</div>