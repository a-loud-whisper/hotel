<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<link href="${pageContext.request.contextPath}/css/register.css" rel="stylesheet" type="text/css" />
<meta charset="utf-8">
<title>登录页面</title>
</head>
<body>
<div class="lir">
<h3><a href="${pageContext.request.contextPath}"><</a>&nbsp;&nbsp;&nbsp;Songtian.com&nbsp;松田</h3>
<br />
<h2>登录</h2>
<h4>登录帐号来使用各种服务。</h4>
	<form action="${pageContext.request.contextPath}/HotelLogin" method = "post">
		<table>
			<tr>
			<td class="left">手机号<br />/用户名</td>
			<td><input type="text" class="txt" name="HG_phone"></td>
			</tr>
			<tr>
			<td class="left">密&nbsp;&nbsp;&nbsp;码  </td>
			<td><input type="password" class="txt" name="HG_passwd"></td>
			</tr>
        </table>
        <c:if test="${!empty requestScope.err}">
				<c:if test="${requestScope.err==-1}">
					<span style='color:#FF0000'>用户身份验证不通过或没审核</span>
				</c:if>
			</c:if>
		<input type="submit" value="登录"  class="radio">
		<input type="reset" value="重置" class="radio">
		<h6>还没有账号？<a href="register.jsp">个人注册</a>&nbsp;&nbsp;<a href="register_hotel.jsp">酒店注册</a></h6>
    </form>
</div> 
<p>登录或注册帐号即代表您同意本公司的<a href="#" class="a">相关条款</a>以及<a href="#" class="a">隐私声明</a></p>
<br /><hr  color="#dbeae8" width="330px"/>
<p>保留所有权利.</p>
<p>版权所有 (2019 - 2020) - Songtian.com™ </p>






</body>
</html>