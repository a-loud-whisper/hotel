<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Insert title here</title>
<link href="${pageContext.request.contextPath}/css/hotel_changeMessage.css" rel="stylesheet" type="text/css">
</head>

<body>
<form method="post" enctype="multipart/form-data" action="${pageContext.request.contextPath}/Hotel_baseChange">
<table class="main1">
<tr onmouseover="this.style.backgroundColor='#96A6A9';" onmouseout="this.style.backgroundColor='#d4e3e5';"><th>酒店账号：</th><td align="left"><input name="HB_id" size="20" type="text" value="${sessionScope.HU_Id}" readonly="readonly"></td></tr>
<tr onmouseover="this.style.backgroundColor='#96A6A9';" onmouseout="this.style.backgroundColor='#d4e3e5';"><th>酒店电话：</th><td align="left"><input name="HB_phone" size="11" value="${requestScope.base.HB_phone}"></td></tr>
<tr onmouseover="this.style.backgroundColor='#96A6A9';" onmouseout="this.style.backgroundColor='#d4e3e5';"><th>酒店邮箱：</th><td align="left"><input name="HB_exmail" size="30" value="${requestScope.base.HB_exmail}"></td></tr>
<tr onmouseover="this.style.backgroundColor='#96A6A9';" onmouseout="this.style.backgroundColor='#d4e3e5';"><th>酒店住址：</th><td align="left"><input name="HB_url" size="45" value="${requestScope.base.HB_url}"></td></tr>
<tr onmouseover="this.style.backgroundColor='#96A6A9';" onmouseout="this.style.backgroundColor='#d4e3e5';"><th>酒店简介：</th><td align="left"><textarea name="HB_profile" cols="100%" rows="6" >${requestScope.base.HB_profile}</textarea></td></tr>
<tr onmouseover="this.style.backgroundColor='#96A6A9';" onmouseout="this.style.backgroundColor='#d4e3e5';"><th>酒店照片：</th><td align="left"><input name="HB_photo" type="file" value="${requestScope.base.HB_photo}"></td></tr>
<tr><td colspan="2" ><input type="submit" value="提交"></td></tr>
</table>
</form>

</body>
</html>