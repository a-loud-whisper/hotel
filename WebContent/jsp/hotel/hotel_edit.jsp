<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Insert title here</title>
<link href="${pageContext.request.contextPath}/css/hotel_addRoom.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="div1">
<form method="post" enctype="multipart/form-data" action="${pageContext.request.contextPath}/RoomAlter">
<table class="main1" >
<tr onmouseover="this.style.backgroundColor='#96A6A9';" onmouseout="this.style.backgroundColor='#d4e3e5';"><th>房间ID:</th><td align="left"><input name="HR_id" size="11" value="${requestScope.room.HR_id}" readonly="readonly"></td></tr>
<tr onmouseover="this.style.backgroundColor='#96A6A9';" onmouseout="this.style.backgroundColor='#d4e3e5';"><th>房间类型:</th><td align="left"><input name="HR_name" size="45" value="${requestScope.room.HR_name}"></td></tr>
<tr onmouseover="this.style.backgroundColor='#96A6A9';" onmouseout="this.style.backgroundColor='#d4e3e5';"><th>房间面积:</th><td align="left"><input name="HR_area" size="3" value="${requestScope.room.HR_area}"></td></tr>
<tr onmouseover="this.style.backgroundColor='#96A6A9';" onmouseout="this.style.backgroundColor='#d4e3e5';"><th>房间床型:</th><td align="left"><input name="HR_bed type" size="20" value="${requestScope.room.HR_bedType}"></td></tr>
<tr onmouseover="this.style.backgroundColor='#96A6A9';" onmouseout="this.style.backgroundColor='#d4e3e5';"><th>房间入住人数:</th><td align="left"><input name="HR_checkin number" size="2" value="${requestScope.room.HR_checkinNumber}"></td></tr>
<tr onmouseover="this.style.backgroundColor='#96A6A9';" onmouseout="this.style.backgroundColor='#d4e3e5';"><th>房间楼层:</th><td align="left"><input name="HR_floor" size="2" value="${requestScope.room.HR_floor}"></td></tr>
<tr onmouseover="this.style.backgroundColor='#96A6A9';" onmouseout="this.style.backgroundColor='#d4e3e5';"><th>房间窗户:</th><td align="left"><select name="HR_window"><option value="有">有</option><option value="无">无</option></select></td></tr>
<tr onmouseover="this.style.backgroundColor='#96A6A9';" onmouseout="this.style.backgroundColor='#d4e3e5';"><th>房间价格:</th><td align="left"><input name="HR_prise" size="30" value="${requestScope.room.HR_prise}"></td></tr>
<tr onmouseover="this.style.backgroundColor='#96A6A9';" onmouseout="this.style.backgroundColor='#d4e3e5';"><th>房间照片:</th><td align="left"><input name="HR_photo" size="30" type="file" value="${requestScope.room.HR_photo}"></td></tr>
<tr><td colspan="2" align="center"><input type="submit" value="修改" ></td></tr>
</table>
</form>
</div>
</body>
</html>