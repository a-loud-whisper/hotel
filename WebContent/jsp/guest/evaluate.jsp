<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link href="${pageContext.request.contextPath}/css/evaluate.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.request.contextPath}/My97DatePicker/WdatePicker.js"></script>
<title>评价页面</title>
</head>
<body>
<div class="ev">
	<h2>评价反馈</h2><br/>
	<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;尊敬的宾客：您好！ 欢迎您入住了本酒店，本酒店作为中高端酒店，
		迎合主流中档商务人士对社交的需要为忙碌的商旅人士打造了脱离紧张浮躁的行程，符合自身内心需求的酒店，本酒店全体员工：欢迎您的光临，祝您入住愉快!</p>
	<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;我是为您服务的员工：小邓子！ 希望您对酒店的服务满意，为了下一次能为您提供更好的服务，请您提出宝贵的意见和建议，感谢您的宝贵意见。</p>
	<br/>
	<form action="" class="form">
			<ul><li class="rleft">房间号</li>
				<li>159</li>
				<li>2019年11月11日</li>
			</ul>
			<table>
				<tr>
					<td><textarea  cols="50" rows="7" class="content"></textarea></td>
				</tr>
			</table><br/>
			<input type="submit" value="提交"  class="radio">
	</form>
</div>
</body>
</html>