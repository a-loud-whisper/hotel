<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link href="${pageContext.request.contextPath}/css/right.css" rel="stylesheet" type="text/css" />
<title>Insert title here</title>
</head>
<body>
	<h2>订单记录</h2>
	<br/>
		<table>
			<tr class="head">
				<td>订单单号</td>
				<td>入住时间</td>
				<td>退房时间</td>
				<td>房型名称</td>
				<td>入住人数</td>
				<td>房间价格</td>
				<td>操作</td>
			</tr>
			<c:forEach items="${requestScope.list}" var="list">
			<tr>
				<td>${list.HI_indentNumber }</td>
				<td>${list.HI_checkinDate }</td>
				<td>${list.HI_departureDate }</td>
				<td>${list.HI_hotelName }</td>
				<td>${list.HI_userPhone }</td>
				<td>${list.HI_prise }</td>
				<c:choose>
				<c:when test="${list.HI_statuse=='退订成功'}">
					<td class="tuifang">已退房</td>
				</c:when>
				<c:otherwise>
					<td><a href="${pageContext.request.contextPath}/guest_hotel_out?number=${list.HI_indentNumber }"><input class="tuifang1" type="submit" value="退房"></a></td>
				</c:otherwise>
				</c:choose>
			</tr>
			</c:forEach>
		</table>
</body>
</html>