<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<link href="${pageContext.request.contextPath}/css/register.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.request.contextPath}/js/register.js" type="text/javascript"></script>
<meta charset="utf-8">
<title>注册页面</title>
</head>
<body>
<div class="lir">
<h3><a href="#"><</a>&nbsp;&nbsp;&nbsp;Songtian.com&nbsp;松田</h3>
<br />
<h2>创建账号</h2>
<h4>创建帐号，轻松使用各种服务。</h4>
	<form action="${pageContext.request.contextPath}/registerHotel" onsubmit="return qqq()">
		<table>
			<tr>
			<td class="left">酒店名</td>
			<td><input type="text" name="HG_phone" class="txt" id="name"></td>
			</tr>
			<tr>
			<td class="left">密&nbsp;&nbsp;&nbsp;码</td>
			<td><input type="password"  id="Pass1" class="txt" onChange="passwd()"></td>
			</tr>
			<tr>
			<td class="left"></td>
			<td class="infer"  id="passwd"></td>
			</tr>
			<tr>
			<td class="left">确认密码</td>
			<td><input type="password" id="Pass2" name="HG_passwd" class="txt" onFocus="pw()" onMouseout="pw()"></td>
			</tr>
			<tr>
			<td class="left"></td>
			<td class="infer"  id="pw"></td>
			</tr>
        </table>
		<input  type="submit" value="注册"  class="radio">
		<input type="reset" value="重置" class="radio">
		<h6>已有账号？<a href="login.jsp">登录</a></h6>
</form>
</div>
<p>登录或注册帐号即代表您同意本公司的<a href="#" class="a">相关条款</a>以及<a href="#" class="a">隐私声明</a></p>
<br /><hr  color="#dbeae8" width="330px"/>
<p>保留所有权利.</p>
<p>版权所有 (2019 - 2020) - Songtian.com™ </p>
</body>
</html>