<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>管理页面</title>
<link rel="stylesheet"type="text/css"href="${pageContext.request.contextPath}/css/Information.css">
</head>
<body>

<div id="headerline">
<h6>
<div id="headerlogo"><img src= "${pageContext.request.contextPath}/images/logo.png" width="100%" height="30"/>&nbsp;&nbsp;&nbsp;&nbsp;当前登录人：${sessionScope.ID}</div>
<div id="headerline_center"></div>
<div id="headerwords"><br><button>首页</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="${pageContext.request.contextPath}/exidSystem"><button>退出</button></a></br></div>
</h6>
</div><hr>

<div id="content">

<div id="content_left">
<div id="content_left_top"></div>
 <div id="content_left_bottom"><p>审核选项：</p>
 <br><a href="${pageContext.request.contextPath}/lookUser?type=hotel_guest"  target="rightFrame"><li>查看个人用户</li></a></br>
 <br><a href="${pageContext.request.contextPath}/lookUser?type=hotel_base"  target="rightFrame"><li>查看酒店用户</li></a></br>
 <br><a href="${pageContext.request.contextPath}/lookUser?type=hotel_user"  target="rightFrame"><li>修改酒店状态</li></a></br>
</div></div>

<div id="content_middle" >
 <iframe width="100%" height="695px" name="rightFrame" id="rightFrame"></iframe>
 
</div>

</div><hr>
</body>
</html>