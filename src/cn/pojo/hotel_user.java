package cn.pojo;

public class hotel_user {
	private String HU_Id;
	private String HU_Passwd;
	private String HU_statuse;
	public String getHU_Id() {
		return HU_Id;
	}
	public void setHU_Id(String hU_Id) {
		HU_Id = hU_Id;
	}
	public String getHU_Passwd() {
		return HU_Passwd;
	}
	public void setHU_Passwd(String hU_Passwd) {
		HU_Passwd = hU_Passwd;
	}
	public String getHU_statuse() {
		return HU_statuse;
	}
	public void setHU_statuse(String hU_statuse) {
		HU_statuse = hU_statuse;
	}
	
}
