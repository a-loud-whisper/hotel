package cn.pojo;

import java.sql.Timestamp;

public class hotel_indent {
	private String HI_guestName;
	private String HI_guestPhone;
	private String HI_guestIdCard;
	private int HI_roomId;
	private double HI_prise;
	private String HI_statuse;
	private Timestamp HI_checkinDate;
	private Timestamp HI_departureDate;
	private String HI_indentNumber;
	private String HI_userPhone;
	private String HI_hotelName;
	public String getHI_guestName() {
		return HI_guestName;
	}
	public void setHI_guestName(String hI_guestName) {
		HI_guestName = hI_guestName;
	}
	public String getHI_guestPhone() {
		return HI_guestPhone;
	}
	public void setHI_guestPhone(String hI_guestPhone) {
		HI_guestPhone = hI_guestPhone;
	}
	public String getHI_guestIdCard() {
		return HI_guestIdCard;
	}
	public void setHI_guestIdCard(String hI_guestIdCard) {
		HI_guestIdCard = hI_guestIdCard;
	}
	public int getHI_roomId() {
		return HI_roomId;
	}
	public void setHI_roomId(int hI_roomId) {
		HI_roomId = hI_roomId;
	}
	public double getHI_prise() {
		return HI_prise;
	}
	public void setHI_prise(double hI_prise) {
		HI_prise = hI_prise;
	}
	public String getHI_statuse() {
		return HI_statuse;
	}
	public void setHI_statuse(String hI_statuse) {
		HI_statuse = hI_statuse;
	}
	public Timestamp getHI_checkinDate() {
		return HI_checkinDate;
	}
	public void setHI_checkinDate(Timestamp hI_checkinDate) {
		HI_checkinDate = hI_checkinDate;
	}
	public Timestamp getHI_departureDate() {
		return HI_departureDate;
	}
	public void setHI_departureDate(Timestamp hI_departureDate) {
		HI_departureDate = hI_departureDate;
	}
	public String getHI_indentNumber() {
		return HI_indentNumber;
	}
	public void setHI_indentNumber(String hI_indentNumber) {
		HI_indentNumber = hI_indentNumber;
	}
	public String getHI_userPhone() {
		return HI_userPhone;
	}
	public void setHI_userPhone(String hI_userPhone) {
		HI_userPhone = hI_userPhone;
	}
	public String getHI_hotelName() {
		return HI_hotelName;
	}
	public void setHI_hotelName(String hI_hotelName) {
		HI_hotelName = hI_hotelName;
	}
	
	
	

}
