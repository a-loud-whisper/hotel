package cn.control;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.dao.hotel_baseDao;
import cn.pojo.hotel_base;

public class Hotel_baseUpdate extends HttpServlet{

@Override
protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	// TODO Auto-generated method stub
	hotel_baseDao basedao = new hotel_baseDao();
	hotel_base base = new hotel_base();
	HttpSession session = req.getSession();
	String id=(String) session.getAttribute("HU_Id");
try {
	base = basedao.queryHotel_Base(id);
	req.setAttribute("base", base);
	
} catch (Exception e) {
	// TODO: handle exception
	e.printStackTrace();
}
    RequestDispatcher rd=req.getRequestDispatcher("jsp/hotel/hotel_changeMessage.jsp");
    rd.forward(req, resp);	
	
}


@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
	       doPost(req, resp);
	}
	
}
