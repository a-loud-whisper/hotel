package cn.control;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.dao.hotel_indentDao;
import cn.pojo.hotel_indent;

@SuppressWarnings("serial")
public class reservation extends HttpServlet {
		@Override
		protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			HttpSession se = req.getSession();
			String hotel_name = (String) se.getAttribute("HU_Id");
			hotel_indentDao text = new hotel_indentDao();
			ArrayList<hotel_indent> list=new  ArrayList<hotel_indent>();
			try {
				list = text.queryHotel_Indent_hotelName(hotel_name);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(list != null) {
				req.setAttribute("indent", list);
				RequestDispatcher rd = req.getRequestDispatcher("jsp/hotel/hotel_reservation.jsp");
				rd.forward(req, resp);
			}
		}
		
		protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
		}
}
