package cn.control;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.dao.DBUtil;

@SuppressWarnings("serial")
public class displayImg extends HttpServlet {
	//servlet 使用方法   
	//<img src="${pageContext.request.contextPath}/displayImg?qqq=表名&type=表中搜索的列名&text=搜索内容&ss=图片的列名">
	public void doGet(HttpServletRequest request, HttpServletResponse response)	throws ServletException, IOException {
		response.setContentType("image/*");
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		Connection con;PreparedStatement pstmt;ResultSet rs=null;
		String aa = request.getParameter("qqq");//表名
		String type =request.getParameter("type");//表中的列名
		String username = request.getParameter("text");//搜索内容
		String xx = request.getParameter("ss");
		String sql="select * from " + aa +  " where " + type +" =?";
		con = DBUtil.getConnection();
		try{
			pstmt =  con.prepareStatement(sql);
			pstmt.setString(1, username);
			rs=pstmt.executeQuery();
			if(rs.next()){
				byte[] buff =rs.getBytes(xx);// p：图片的列名 
				OutputStream os = response.getOutputStream();
				os.write(buff);
			}
			pstmt.close();
			rs.close();
		}catch(Exception e){
		}
	}  
 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		doGet(request, response);
	}
}
