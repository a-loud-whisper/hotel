package cn.control;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.dao.hotel_guestDao;
import cn.dao.hotel_indentDao;
import cn.pojo.hotel_guest;

public class GuestUPphone extends HttpServlet {

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String phone = req.getParameter("phone");
		String newphone =req.getParameter("newTele");
		hotel_guestDao guestdao = new hotel_guestDao();
		hotel_guest guest=new hotel_guest();
		guest = guestdao.queryHotel_Guest(phone);
		hotel_guest newguest = new hotel_guest();
		newguest.setHG_name(guest.getHG_name());
		newguest.setHG_sex(guest.getHG_sex());
		newguest.setHG_passwd(guest.getHG_passwd());
		newguest.setHG_phone(newphone);
		newguest.setHG_exmail(guest.getHG_exmail());
		newguest.setHG_idcard(guest.getHG_idcard());
		hotel_indentDao indendao = new hotel_indentDao();
		boolean aa = false;
		try {
			aa=guestdao.addHotel_Guest(newguest);
			boolean cc = guestdao.deleteguest(phone);
			boolean dd = indendao.alterHotel_guestindent(phone, newphone);
			
			resp.sendRedirect("Hotel_All");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
