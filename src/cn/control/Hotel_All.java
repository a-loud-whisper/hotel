package cn.control;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.dao.hotel_baseDao;
import cn.pojo.hotel_base;


public class Hotel_All extends HttpServlet{
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ArrayList<hotel_base> userAll = new ArrayList<hotel_base>();
		hotel_baseDao userdao = new hotel_baseDao();
		try {
			userAll = userdao.allHotel_Base();
			int count = userAll.size();
			req.setAttribute("userAll", userAll);
			req.setAttribute("count",count);
			RequestDispatcher rd=req.getRequestDispatcher("jsp/main.jsp");
	    	rd.forward(req, resp);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
}
