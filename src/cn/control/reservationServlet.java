package cn.control;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.dao.hotel_indentDao;
import cn.pojo.hotel_indent;

public class reservationServlet extends HttpServlet {   
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
  	  hotel_indentDao queryH_Indent=new hotel_indentDao();
  	  hotel_indent query=new hotel_indent();
  	  
  	/*  String HI_checkinDate = request.getParameter("HI_checkinDate");
  	  SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  	  java.util.Date date = null;
		try {
			date = formatter.parse(HI_checkinDate);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
  	  java.sql.Date HI_checkinDate1 = new java.sql.Date(date.getTime()); 
  	  
  	  String HI_departureDate = request.getParameter("HI_departureDate");
  	  SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  	  java.util.Date date1 = null;
		try {
			date1 = formatter1.parse(HI_departureDate);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
  	  java.sql.Date HI_departureDate1 = new java.sql.Date(date1.getTime());
  */	  
  	query.setHI_guestName(request.getParameter("HI_guestName"));
  	query.setHI_guestPhone(request.getParameter("HI_guestPhone"));
  	query.setHI_guestIdCard(request.getParameter("HI_guestIdCard"));
  //	query.setHI_roomId(Integer.parseInt(request.getParameter("HI_roomId")));
  //	query.setHI_prise(Double.parseDouble(request.getParameter("HI_prise")));
  	query.setHI_statuse(request.getParameter("HI_statuse"));
  //	query.setHI_checkinDate(HI_checkinDate1);
  //	query.setHI_departureDate(HI_departureDate1);
  //	query.setHI_indentNumber(Integer.parseInt(request.getParameter("HI_indentNumber")));
  //	query.setHI_userPhone(Integer.parseInt(request.getParameter("HI_userPhone")));
  	query.setHI_hotelName(request.getParameter("HI_hotelName"));
  	ArrayList<hotel_indent> list=new  ArrayList<hotel_indent>();
	 try {
		list = queryH_Indent.queryHotel_Indent_indentNumber("query");
	     } catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	      }
	 
	 if(list!=null) {
    	 RequestDispatcher rd= request.getRequestDispatcher("/jsp/hotel/hotel_reservation.jsp");
         
         rd.forward(request, response);
         }else{
        	    
         	RequestDispatcher rd= request.getRequestDispatcher("BookJS.jsp");
             
             rd.forward(request, response);
         }
     }
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 doGet(request, response);
}
}
