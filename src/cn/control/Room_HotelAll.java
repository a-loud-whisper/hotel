package cn.control;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.dao.hotel_baseDao;
import cn.dao.hotel_roomDao;
import cn.pojo.hotel_base;
import cn.pojo.hotel_room;

@SuppressWarnings("serial")
public class Room_HotelAll extends HttpServlet{
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ArrayList<hotel_room> list = new ArrayList<hotel_room>();
		hotel_roomDao roomdao = new hotel_roomDao();
		String hotelName = req.getParameter("hotelName");
		try {
			hotel_base base = new hotel_base();
			hotel_baseDao basedao = new hotel_baseDao();
			base=basedao.queryHotel_Base(hotelName);
			req.setAttribute("base", base);
			list = roomdao.queryAllRoom(hotelName);
			req.setAttribute("list", list);
			req.setAttribute("hotelName", hotelName);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		RequestDispatcher rd=req.getRequestDispatcher("jsp/Room.jsp");
		rd.forward(req, resp);	
		
	}
	
	
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

}
