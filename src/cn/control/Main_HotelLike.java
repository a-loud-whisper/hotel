package cn.control;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.dao.hotel_baseDao;
import cn.pojo.hotel_base;

public class Main_HotelLike extends HttpServlet{
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String hotelname=req.getParameter("hotelname");
		ArrayList<hotel_base> list = new ArrayList<hotel_base>();
		hotel_base hotel = new hotel_base();
		hotel_baseDao hoteldao = new hotel_baseDao();
		try {
			HttpSession session = req.getSession();
			session.setAttribute("hotelname", hotelname);	
			list=hoteldao.likeHotel_Base(hotelname);
			req.setAttribute("userAll", list);
			RequestDispatcher rd=req.getRequestDispatcher("jsp/main.jsp");
	    	rd.forward(req, resp);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

}
