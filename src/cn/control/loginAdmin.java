package cn.control;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.dao.hotel_systemDao;

@SuppressWarnings("serial")
public class loginAdmin extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		hotel_systemDao qq = new hotel_systemDao();
		boolean aa = false;
		HttpSession se = req.getSession();
		String ID = req.getParameter("sUserId");
		String PW = req.getParameter("sPasswd");
		try {
			aa = qq.judgeSystem(ID, PW);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(aa == true) {
			se.setAttribute("ID", ID);
			RequestDispatcher rd = req.getRequestDispatcher("jsp/system/Administrator.jsp");
			rd.forward(req, resp);
		}
		else {
			resp.sendRedirect("jsp/system/loginAdmin.jsp");
		}
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}
}