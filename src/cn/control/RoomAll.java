package cn.control;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.dao.hotel_roomDao;
import cn.pojo.hotel_room;

public class RoomAll extends HttpServlet{

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ArrayList<hotel_room> all=new ArrayList<hotel_room>();
		hotel_roomDao room=new hotel_roomDao();
		try {
			HttpSession session = req.getSession();
			String id=(String) session.getAttribute("HU_Id");
			all=room.queryAllRoom(id);
			req.setAttribute("allroom", all);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		RequestDispatcher rd=req.getRequestDispatcher("jsp/hotel/hotel_room.jsp");
		rd.forward(req, resp);
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
}
