package cn.control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.dao.hotel_guestDao;
import cn.pojo.hotel_guest;

public class Guest_base extends HttpServlet{
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String phone = req.getParameter("phone");
		hotel_guest guest = new hotel_guest();
		hotel_guestDao guestdao = new hotel_guestDao();
		guest = guestdao.queryHotel_Guest(phone);
		req.setAttribute("guest", guest);
		RequestDispatcher rd=req.getRequestDispatcher("jsp/guest/hotel_guest.jsp");
		rd.forward(req, resp);	
		
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
}
