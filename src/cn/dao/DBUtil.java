package cn.dao;

import java.sql.Connection;
import java.sql.DriverManager;


public class DBUtil {
	static {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
		public static Connection getConnection() {
			Connection con = null;
			 String DB_URL = "jdbc:mysql://localhost:3306/outsourcing_website?useSSL=false&serverTimezone=Asia/Shanghai&allowPublicKeyRetrieval=true";
		     String DB_USERNAME = "root";
		     String DB_PASSWORD = "qwaszx147";
			 try {	
		            con = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
					 
				} catch (Exception e) {
					e.printStackTrace();
				}
			
			return con;
		}
	

}
