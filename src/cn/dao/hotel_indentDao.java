package cn.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import cn.pojo.hotel_indent;
import cn.pojo.hotel_room;
import cn.dao.DBUtil;


public class hotel_indentDao  extends DBUtil{
	
	     public boolean addHotel_Indent(hotel_indent indent) throws SQLException{            //增加订单信息
	    	 Connection conn=DBUtil.getConnection();
	    	 boolean f=false;  PreparedStatement pstmt=null; int a=0;
	    	 String add_sql="insert into hotel_indent values(?,?,?,?,?,?,?,?,?,?,?)";
	    	 pstmt=conn.prepareStatement(add_sql);
	    	 pstmt.setString(1, indent.getHI_indentNumber());
	    	 pstmt.setInt(2,indent.getHI_roomId());
	         pstmt.setString(3,indent.getHI_guestName());
	    	 pstmt.setString(4,indent.getHI_guestPhone());
	    	 pstmt.setString(5,indent.getHI_guestIdCard());
	    	 pstmt.setTimestamp(6,indent.getHI_checkinDate());
	    	 pstmt.setTimestamp(7,indent.getHI_departureDate());
	    	 pstmt.setDouble(8,indent.getHI_prise());
	    	 pstmt.setString(9,indent.getHI_statuse());
	    	 pstmt.setString(10,indent.getHI_hotelName());
	    	 pstmt.setString(11,indent.getHI_userPhone());	
	    	 a=pstmt.executeUpdate();
	    	 if(a!=0) {
	    		 f=true;
	    	 }
	    	 conn.close();
	    	 pstmt.close();
	    	 return f;
	     }
	     
	     public boolean alterHotel_Indent(String aa,String bb) throws SQLException {      //修改订单信息    获得整个订单来更新    
	    	   Connection conn=DBUtil.getConnection();
	    	   boolean f=false;  PreparedStatement pstmt=null; int a=0;
	    	   String alter_sql="update hotel_indent set HI_statuse=? where HI_indentNumber=?";
	    	   pstmt=conn.prepareStatement(alter_sql);
	    	   pstmt.setString(1,aa);
	    	   pstmt.setString(2,bb);
	    	   a=pstmt.executeUpdate();
	    	   if(a!=0) {
	    		   f=true;
	    	   }
		       conn.close();
		       pstmt.close();
			   return f;
	       }

	     
	     public ArrayList<hotel_indent> queryHotel_Indent_indentNumber(String indentNumber) throws SQLException{      //通过indentNumber获得订单信息
	    	  Connection conn=DBUtil.getConnection();
	    	  PreparedStatement pstmt=null; hotel_indent queryhotel=null;
	    	  String query_sql="select *from hotel_indent where HI_indentNumber=?";
	    	  pstmt=conn.prepareStatement(query_sql);
	    	  pstmt.setString(1, indentNumber);
	    	  ArrayList<hotel_indent> list=new  ArrayList<hotel_indent>();
	    	  ResultSet rs=pstmt.executeQuery();
	    	  if(rs!=null && rs.next()) {
	    		  queryhotel = new hotel_indent();
	    		  queryhotel.setHI_guestName(rs.getString("hI_guestName"));
	    		  queryhotel.setHI_guestPhone(rs.getString("hI_guestPhone"));
	    		  queryhotel.setHI_guestIdCard(rs.getString("hI_guestIdCard"));
	    		  queryhotel.setHI_roomId(rs.getInt("hI_roomId"));
	    		  queryhotel.setHI_prise(rs.getDouble("hI_prise"));
	    		  queryhotel.setHI_statuse(rs.getString("hI_statuse"));
	    		  queryhotel.setHI_checkinDate(rs.getTimestamp("hI_checkinDate"));
	    		  queryhotel.setHI_departureDate(rs.getTimestamp("hI_departureDate"));
	    		  queryhotel.setHI_indentNumber(rs.getString("hI_indentNumber"));
	    		  queryhotel.setHI_userPhone(rs.getString("hI_userPhone"));
	    		  queryhotel.setHI_hotelName(rs.getString("hI_hotelName"));
	    		  list.add(queryhotel);
	    	  }
	    	  rs.close();
	    	  conn.close();
	    	  pstmt.close();
	      return list;
}
	          
	      public ArrayList<hotel_indent> queryHotel_Indent_guestPhone(String guestPhone) throws SQLException{      //通过guestPhone获得订单信息
	    	  Connection conn=DBUtil.getConnection();
	    	  PreparedStatement pstmt=null; hotel_indent queryhotel=null;
	    	  String query_sql="select *from hotel_indent where hI_userPhone=?";
	    	  pstmt=conn.prepareStatement(query_sql);
	    	  pstmt.setString(1, guestPhone);
	    	  ArrayList<hotel_indent> list=new  ArrayList<hotel_indent>();
	    	  ResultSet rs=pstmt.executeQuery();
	    	  if(rs!=null && rs.next()) {
	    		  queryhotel = new hotel_indent();
	    		  queryhotel.setHI_guestName(rs.getString("hI_guestName"));
	    		  queryhotel.setHI_guestPhone(rs.getString("hI_guestPhone"));
	    		  queryhotel.setHI_guestIdCard(rs.getString("hI_guestIdCard"));
	    		  queryhotel.setHI_roomId(rs.getInt("hI_roomId"));
	    		  queryhotel.setHI_prise(rs.getDouble("hI_prise"));
	    		  queryhotel.setHI_statuse(rs.getString("hI_statuse"));
	    		  queryhotel.setHI_checkinDate(rs.getTimestamp("hI_checkinDate"));
	    		  queryhotel.setHI_departureDate(rs.getTimestamp("hI_departureDate"));
	    		  queryhotel.setHI_indentNumber(rs.getString("hI_indentNumber"));
	    		  queryhotel.setHI_userPhone(rs.getString("hI_userPhone"));
	    		  queryhotel.setHI_hotelName(rs.getString("hI_hotelName"));
	    		  list.add(queryhotel);
	    	  }
	    	  rs.close();
	    	  conn.close();
	    	  pstmt.close();
	      return list;
	      }
	      
	      public ArrayList<hotel_indent> queryHotel_Indent_hotelName(String hotelName) throws SQLException{      //通过hotelName获得订单信息
	    	  Connection conn=DBUtil.getConnection();
	    	  PreparedStatement pstmt=null; hotel_indent queryhotel=null;
	    	  String query_sql="select * from hotel_indent where HI_hotelName = ?";
	    	  pstmt=conn.prepareStatement(query_sql);
	    	  pstmt.setString(1, hotelName);
	    	  ArrayList<hotel_indent> list=new  ArrayList<hotel_indent>();
	    	  ResultSet rs=pstmt.executeQuery();
	    	  if(rs!=null && rs.next()) {
	    		  queryhotel = new hotel_indent();
	    		  queryhotel.setHI_guestName(rs.getString("hI_guestName"));
	    		  queryhotel.setHI_guestPhone(rs.getString("hI_guestPhone"));
	    		  queryhotel.setHI_guestIdCard(rs.getString("hI_guestIdCard"));
	    		  queryhotel.setHI_roomId(rs.getInt("hI_roomId"));
	    		  queryhotel.setHI_prise(rs.getDouble("hI_prise"));
	    		  queryhotel.setHI_statuse(rs.getString("hI_statuse"));
	    		  queryhotel.setHI_checkinDate(rs.getTimestamp("hI_checkinDate"));
	    		  queryhotel.setHI_departureDate(rs.getTimestamp("hI_departureDate"));
	    		  queryhotel.setHI_indentNumber(rs.getString("hI_indentNumber"));
	    		  queryhotel.setHI_userPhone(rs.getString("hI_userPhone"));
	    		  queryhotel.setHI_hotelName(rs.getString("hI_hotelName"));
	    		  list.add(queryhotel);
	    	  }
	    	  rs.close();
	    	  conn.close();
	    	  pstmt.close();
	      return list;
	      }
	      public boolean alterHotel_guestindent(String phone,String newphone) throws SQLException { //个人修改手机号得订单
	  		Connection conn=null;PreparedStatement pstmt=null;int rs=0;boolean aa=false;
	  		conn=DBUtil.getConnection();
	  		String sql="update hotel_room set HI_userPhone=? where HI_userPhone=?";
	  		pstmt=conn.prepareStatement(sql);
	  		pstmt.setString(1,newphone);
	  		pstmt.setString(2,phone);
	  		rs = pstmt.executeUpdate();
	  		if(rs==0)aa=false;
	  		else aa=true;
	  		pstmt.close();
	  		conn.close();		
	  		return aa;
	  	}
	      public boolean updateHotel_guestindent(String number,String aa) throws SQLException { //通过订单编号修改订单状态
		  		Connection conn=null;PreparedStatement pstmt=null;int rs=0;boolean a=false;
		  		conn=DBUtil.getConnection();
		  		String sql="update hotel_indent set HI_statuse=? where HI_indentNumber=?";
		  		pstmt=conn.prepareStatement(sql);
		  		pstmt.setString(1, aa);
		  		pstmt.setString(2,number);
		  		rs = pstmt.executeUpdate();
		  		if(rs==0)a=false;
		  		else a=true;
		  		pstmt.close();
		  		conn.close();		
		  		return a;
		  	}
	}

